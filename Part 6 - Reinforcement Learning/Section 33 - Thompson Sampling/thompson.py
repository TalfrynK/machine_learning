import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random

dataset = pd.read_csv("Ads_CTR_Optimisation.csv")

d = dataset.shape[1]

select_ads = []
reward_num_1 = [0] *d
reward_num_0 = [0] * d
reward_total = 0

for n in range (0, dataset.shape[0]):
    max_random = 0
    ad = 0
    for i in range (0, d):
        random_beta = random.betavariate(reward_num_1[i] + 1, reward_num_0[i] + 1)
        if random_beta > max_random:
            max_random = random_beta
            ad = i
    select_ads.append(ad)
    reward = dataset.values[n, ad]
    if reward == 1:
        reward_num_1[ad] = reward_num_1[ad] + 1
    else:
        reward_num_0[ad] = reward_num_0[ad] + 1
    reward_total = reward_total + reward

plt.hist(select_ads)
plt.title("Histogram of Ad Selections")
plt.xlabel("Ads")
plt.ylabel("Number of Times Selected")