import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math

dataset = pd.read_csv("Ads_CTR_Optimisation.csv")

d = dataset.shape[1]

select_ads = []
selection_num = [0] * d
reward_sum = [0] * d
reward_total = 0

for n in range (0, dataset.shape[0]):
    max_UB = 0
    ad = 0
    for i in range (0, d):
        if selection_num[i] > 0:
            reward_avg = reward_sum[i] / selection_num[i]
            delta_i = math.sqrt(3/2 * math.log(n + 1) / selection_num[i])
            upper_bound = reward_avg + delta_i
        else:
            upper_bound = 1e400
        if upper_bound > max_UB:
            max_UB = upper_bound
            ad = i
    select_ads.append(ad)
    selection_num [ad] = selection_num[ad] + 1
    reward = dataset.values[n, ad]
    reward_sum[ad] = reward_sum[ad] + reward
    reward_total = reward_total + reward

plt.hist(select_ads)
plt.title("Histogram of Ad Selections")
plt.xlabel("Ads")
plt.ylabel("Number of Times Selected")