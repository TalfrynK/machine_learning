# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#Importing the Libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#Importing the Dataset
dataset = pd.read_csv('Data.csv')
x = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 3].values

#Missing Data
from sklearn.preprocessing import Imputer #Machine Learning Model class import
imputer = Imputer(missing_values = 'NaN', strategy = 'mean', axis = 0) #Setting how to deal with the missing data
imputer = imputer.fit(x[:, 1:3]) #Fitting the object to the matrix
x[:, 1:3] = imputer.transform(x[:, 1:3]) #Fixing the missing data with the mean