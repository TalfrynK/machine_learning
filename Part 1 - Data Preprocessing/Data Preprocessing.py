# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#Importing the Libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#Importing the Dataset
dataset = pd.read_csv('Data.csv')
x = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 3].values

#Missing Data
from sklearn.preprocessing import Imputer #Machine Learning Model class import
imputer = Imputer(missing_values = 'NaN', strategy = 'mean', axis = 0) #Setting how to deal with the missing data
imputer = imputer.fit(x[:, 1:3]) #Fitting the object to the matrix
x[:, 1:3] = imputer.transform(x[:, 1:3]) #Fixing the missing data with the mean

#Encoding categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder #Category Encoding tools
labelencoder_x = LabelEncoder() #Creating object
x[:, 0] = labelencoder_x.fit_transform(x[:, 0]) #Assigning values
onehotencoder = OneHotEncoder(categorical_features = [0]) #Creating object
x = onehotencoder.fit_transform(x).toarray() #3 Columns for Country codes
labelencoder_y = LabelEncoder() #Creating object
y = labelencoder_y.fit_transform(y) #Assigning value for dependant variable

#Splitting the Dataset into Training and Test sets
from sklearn.model_selection import train_test_split #Import the splitting library
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state = 0) #The test and train for the x matrix and y dependant

#Feature Scaling
from sklearn.preprocessing import StandardScaler #Import library for Scaling
sc_x = StandardScaler() #Creating the scaler class
x_train = sc_x.fit_transform(x_train) #Scaling the training set
x_test = sc_x.transform(x_test) #Scaling the test set, dependant variable does not require scaling right now