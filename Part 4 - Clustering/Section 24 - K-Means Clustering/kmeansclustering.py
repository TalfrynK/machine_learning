import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random

#Importing the mall dataset with Pandas
dataset = pd.read_csv("Mall_Customers.csv")
X = dataset.iloc[:, [3,4]].values
df = X.copy()

#Using the Silhouette Score Method to determine the optimal number of clusters
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
score = 0
cluster_range = [1,2,3,4,5,6,7,8,9,10]
for i in range(1,10):
    kmeans = KMeans(n_clusters = cluster_range[i], init = 'k-means++', max_iter = 300, n_init = 10, random_state = 0)
    kmeans.fit(X)
    temp = (silhouette_score(X, kmeans.labels_))
    if temp > score:
        score = temp
        clusterNum = cluster_range[i]
    else:
        score = score
        clusterNum = clusterNum

print ("Number of Clusters:",clusterNum)
print ("Silhouette Score:", score)

#Applying K-Means to the mall dataset
kmeans = KMeans(n_clusters = clusterNum, init = 'k-means++', max_iter = 300, n_init = 10, random_state = 0)
y_kmeans = kmeans.fit_predict(X)

#Visualising the clusters
#Can make this looped
for i in range(0, clusterNum):
    str_i = 'Cluster ' + str(i + 1)
    colour =["#"+''.join([random.choice('0123456789ABCDEF') for b in range(6)])]
    print (colour)
    plt.scatter(X[y_kmeans == i, 0], X[y_kmeans == i, 1], s = 100, c = colour, label = str_i)

plt.scatter(kmeans.cluster_centers_[:,0], kmeans.cluster_centers_[:,1], s = 300, c = 'yellow', label = 'Centroids')
plt.title('Clusters of Clients')
plt.xlabel('Annual Income in Thousands of Dollars ($)')
plt.ylabel('Spending Score (1-100)')
plt.legend()
plt.show()