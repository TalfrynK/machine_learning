K-Means Clustering

K-Means works with multiple dimensions (>2)

Algorithm:
1. Choose the number (K) of clusters
2. Select at random (K) points, called centroids, that don't have to be part of the dataset
3. Assign each data point to the closest centroid, forming K number of clusters
4. Compute and place the new centroid of each cluster (in the center)
5. Reassign each data point to the new closest centroid, if any reassignment took place, go back to Step 4, otherwise finish

Random Initialization Trap

Centroids are chosen poorly
Use the K-Means ++ Algorithm

Deciding the number K of clusters

WCSS formula (Within Clusters Sum of Squares)
Use The Elbow Method to determine the optimal number where the drop is not substantial and it starts to flatten


